////////////////////////////////////////////////////////////
//
// Sprite Animation System
// Copyright (C) 2009-2011 Josh Brown (josh@facelessj.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

/**
 * @file
 * @author Josh Brown
 * @brief Handles a single animation sequence. Holds all the frames for that sequence and sends them to the Animation object. Access to this class
 * is needed only by the Animation class (That is, game objects should interact only with the Animation class, not this AnimationStrip class)
 */

#ifndef _ANIMATIONSTRIP_H_
#define _ANIMATIONSTRIP_H_

#include <vector>

/** SFML Specific Code.
 * If you wish to change the rect to a different type, just make sure the new type has the following constructor
 * constructor(int x1, int y1, int x2, int y2).
 * Don't forget to change the typedef in animation.hpp as well
 */
#include <SFML/Graphics.hpp>
typedef sf::IntRect Rect;

/**
 * @brief Holds a collection of Rects definining the different frames for use in a single animation
 */
class AnimationStrip
{
private:
	/** Individual frame information. Stores the (w, h) , not (x2, y2)  */
	std::vector<Rect> frames;
	/** Individual frame coordinates. Stores the (x2, y2) information in the (width, height) of the rect */
	std::vector<Rect> frameCoords;
	/** Which frame the animation strip is up to */
	int frameCounter;
	/** How many times the animation has been played through */
	int numTimesAnimationPlayed;
public:
	/**
	 * Default constructor
	 */
	AnimationStrip();

	/**
	 * Polls how many frames are in this animation
	 * @return Number of frames
	 */
	int getNumFrames();

	/**
	 * Instantiates the animation strip from a texture object
	 * @param x - Starting position
	 * @param y - Starting position
	 * @param frameWidth - Width of each frame (constant over all frames)
	 * @param frameHeight - Height of each frame (constant over all frames)
	 * @param numFrames - Number of frames to grab
	 */
	void loadAnimationStrip(int x, int y, int frameWidth, int frameHeight, int numFrames);

	/**
	 * Returns the specified frame
	 * @param index - Frame to retrieve. If out of bounds, retrieves first frame
	 * @return Frame rect information, in terms of (x, y, w, h)
	 */
	Rect getFrame(int index);
		
	/**
	 * Increments the frame counter and returns the new frame information
	 * @return Frame rect information, in terms of (x, y, w, h)
	 */
	Rect getNextFrame();

	/**
	 * Returns the current frame
	 * @return Frame rect information, in terms of (x, y, w, h)
	 */
	Rect getCurrentFrame();


	/**
	 * Returns the specified frame
	 * @param index - Frame to retrieve. If out of bounds, retrieves first frame
	 * @return Frame rect information, in terms of (x1, y1, x2, y2)
	 */
	Rect getFrameCoord(int index);
		
	/**
	 * Increments the frame counter and returns the new frame information
	 * @return Frame rect information, in terms of (x1, y1, x2, y2)
	 */
	Rect getNextFrameCoord();

	/**
	 * Returns the current frame
	 * @return Frame rect information, in terms of (x1, y1, x2, y2)
	 */
	Rect getCurrentFrameCoord();

	/**
	 * Resets the frame counter to 0
	 */
	void resetStrip();

	/**
	 * Advances the frame counter to specific frame
	 * @param index - Frame to jump to (If greater than number of frames, sets to 0)
	 */
	void seekStrip(int index);

	/**
	 * Advances frame counter to next frame. If past end of frames, goes back to frame 0
	 */
	void advanceStrip(bool backwards);

	/**
	 * Retrieves the current frame counter
	 * @return Frame counter
	 */
	int getFrameCounter();

	/**
	 * Retrieves the number of times the animation has played through completely
	 * @return Number of times the animation has played
	 */
	int numTimesPlayed();

	/**
	 * Resets the playthrough count
	 */
	void resetPlayCount();
};

#endif //_ANIMATIONSTRIP_H_