#include "animationstrip.hpp"

AnimationStrip::AnimationStrip()
{
	frameCounter = 0;
	numTimesAnimationPlayed = 0;
}

void AnimationStrip::loadAnimationStrip(int x, int y, int frameWidth, int frameHeight, int numFrames)
{
	int x1Pos, y1Pos, x2Pos, y2Pos;
	for(int i = 0; i < numFrames; i++)
	{
		x1Pos = x + i * frameWidth;
		y1Pos = y;
		x2Pos = x + (i + 1) * frameWidth;
		y2Pos = y + frameHeight;
		frames.push_back(Rect(x1Pos, y1Pos, frameWidth, frameHeight));
		frameCoords.push_back(Rect(x1Pos, y1Pos, x2Pos, y2Pos));
	}
}

Rect AnimationStrip::getCurrentFrame()
{
	return frames[frameCounter];
}

Rect AnimationStrip::getNextFrame()
{
	frameCounter++;
	if(frameCounter >= (signed)frames.size())
		frameCounter = 0;
	return frames[frameCounter];
}

Rect AnimationStrip::getFrame(int index)
{
	if(index >= 0 && index < (signed)frames.size())
		return frames[index];
	return frames[0];
}

Rect AnimationStrip::getCurrentFrameCoord()
{
	return frameCoords[frameCounter];
}

Rect AnimationStrip::getNextFrameCoord()
{
	frameCounter++;
	if(frameCounter >= (signed)frames.size())
		frameCounter = 0;
	return frameCoords[frameCounter];
}

Rect AnimationStrip::getFrameCoord(int index)
{
	if(index >= 0 && index < (signed)frames.size())
		return frameCoords[index];
	return frameCoords[0];
}

void AnimationStrip::resetStrip()
{
	frameCounter = 0;
}

void AnimationStrip::seekStrip(int index)
{
	if(index >=0 && index < (signed)frames.size())
		frameCounter = index;
	else
		frameCounter = 0;
}

void AnimationStrip::advanceStrip(bool backwards)
{
	if(backwards)
	{
		frameCounter--;
		if(frameCounter < 0)
		{
			frameCounter = (signed)frames.size() - 1;
			numTimesAnimationPlayed++;
		}
	}
	else
	{
		frameCounter++;
		if(frameCounter >= (signed)frames.size())
		{
			frameCounter = 0;
			numTimesAnimationPlayed++;
		}
	}
}

int AnimationStrip::getFrameCounter()
{
	return frameCounter;
}

int AnimationStrip::getNumFrames()
{
	return frames.size();
}

int AnimationStrip::numTimesPlayed() 
{ 
	return numTimesAnimationPlayed; 
}

void AnimationStrip::resetPlayCount()
{ 
	numTimesAnimationPlayed = 0; 
}