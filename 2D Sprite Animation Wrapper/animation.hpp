////////////////////////////////////////////////////////////
//
// Sprite Animation System
// Copyright (C) 2009-2011 Josh Brown (josh@facelessj.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////


/**
 * @file
 * @author Josh Brown
 * @brief Handles storing animation information. This system DOES NOT store any textures, sprites or image data. It only stores the coordinates of subregions of
 * an image where the animation frames are. This system was written for use primarily with SFML, however should work fine with other APIs.
 *
 * All end user interaction happens through the Animation class. Users don't need to worry about interacting with the AnimationStrip class
 *
 * Example Usage:
 * //Initialisation
 * int animation1, animation2;
 * Animation myAnim;
 * animation1 = myAnim.loadStrip(x1, y1, w1, h1, num1);
 * animation2 = myAnim.loadStrip(x2, y2, w2, h2, num2);
 * myAnim.setCurrentAnimation(animation1);
 * myAnim.setAnimationDuration(1.0f);
 * ...
 * //Update cycle of game
 * myAnim.update(timeInSecondsSinceLastUpdate);
 * ...
 * //Render cycle of game
 * //Frame info can be used to set the texture rect of an sf::Sprite, or used to fill in the texCoords of an sf::Vertex for use in an sf::VertexArray
 * Rect spriteFrameInfo = myAnim.getFrame();
 * mySprite.setTextureRect(spriteFrameInfo);
 *
 * TexCoord texCoordFrameInfo[4];
 * myAnim.getFrameCoord(texCoordFrameInfo);
 * myVertices.append(sf::Vertex(topLeftCorner, texCoordFrameInfo[0]);
 * myVertices.append(sf::Vertex(bottomLeftCorner, texCoordFrameInfo[1]);
 * myVertices.append(sf::Vertex(bottomRightCorner, texCoordFrameInfo[2]);
 * myVertices.append(sf::Vertex(topRightCorner, texCoordFrameInfo[3]);
 */

#ifndef _ANIMATION_H_
#define _ANIMATION_H_

/** SFML Specific Code.
 * If you wish to change the rect to a different type, just make sure the new type has the following constructor
 * constructor(int x1, int y1, int x2, int y2).
 * TexCoord is just a simple 2D vector storing 2 floats.
 * Don't forget to change the typedef in animationstrip.hpp as well
 */
#include <SFML/Graphics.hpp>
typedef sf::IntRect Rect;
typedef sf::Vector2f TexCoord;


/** Forward Declarations */
class AnimationStrip;

/**
 * @brief Holds a collection of animation strips and provides functions to play/pause/manipulate the animation
 */
class Animation
{
private:
	/** Collection of animation strips that can be used by this animation */
	std::vector<AnimationStrip*> strips;

	/** The current animation strip being used */
	int currentAnimation;
	/** Over what time should the entire animation be played */
	float duration;
	/** Timer tracking when next frame should be loaded */
	float timer;
	/** Time per frame */
	float frameTime;

	/** Flag indicating if the animation has been paused */
	bool paused;

	/** Flag indicating if the animation should be playing backwards. Note, getting to
	 *  the end of an animation when playing backwards will still INcrement the play count */
	bool backwards;
public:
	/**
	 * Default constructor
	 */
	Animation();

	/**
	 * Copy constructor
	 */
	Animation(const Animation& rhs);
	Animation& operator = (const Animation& rhs);

	~Animation();

	/**
	 * Create a new animation strip from a texture file and return the reference index for that animation
	 * @param x - Starting position to grab frames from
	 * @param y - Starting position to grab frames from
	 * @param frameWidth - Width of each frame to grab
	 * @param frameHeight - Height of each frame to grab
	 * @param numFrames - Number of frames to grab
	 * @return Reference number for the newly loaded animation
	 */
	int loadStrip(int x, int y, int frameWidth, int frameHeight, int numFrames);

	/**
	 * Stops the animation from updating to the next frame
	 */
	void pause();

	/**
	 * Causes the animation to resume updating frames
	 */
	void resume();

	/**
	 * Gets the current frame information that the game object should draw
	 * @return Frame information for current frame, in terms of (x, y, w, h)
	 */
	Rect getFrame();

	/**
	 * Gets the frame information that the game object should draw
	 * @param index - Frame number to retrieve. If out of bounds, returns the first frame
	 * @return Frame information for specified frame, in terms of (x, y, w, h)
	 */
	Rect getFrame(int index);

	/**
	 * Updates the frame counter, regardless of timer, and returns the new frame information
	 * @return Frame information for next frame, in terms of (x, y, w, h)
	 */
	Rect getNextFrame();

	/**
	 * Gets the current frame information that the game object should draw
	 * @return All 4 coordinates for current frame. Stored CCW from top left corner
	 */
	void getFrameCoord(TexCoord coords[4]);

	/**
	 * Gets the frame information that the game object should draw
	 * @param index - Frame number to retrieve. If out of bounds, returns the first frame
	 * @return All 4 coordinates for specified frame. Stored CCW from top left corner
	 */
	void getFrameCoord(TexCoord coords[4], int index);

	/**
	 * Updates the frame counter, regardless of timer, and returns the new frame information
	 * @return All 4 coordinates for next frame. Stored CCW from top left corner
	 */
	void getNextFrameCoord(TexCoord coords[4]);

	/**
	 * Updates the animation to the next frame if it should be (according to duration timer)
	 * @param dt - Time since last frame
	 */
	void update(float dt);

	/**
	 * Sets the duration to draw the ENTIRE animation
	 * @param seconds - Time in seconds the entire animation should last
	 */
	void setAnimationDuration(float seconds);

	/**
	 * Polls how long the animation lasts
	 * @return Length of animation sequence
	 */
	float getAnimationDuration();

	/**
	 * Polls if the animtion is paused
	 * @return True if paused
	 */
	bool isPaused();

	/**
	 * Polls what animation is currently playing
	 * @return Reference index for the animation
	 */
	int getCurrentAnimation();

	/**
	 * Sets new animation sequence
	 * @param index - Reference index that was given when loadStrip was called
	 */
	void setCurrentAnimation(int index);

	/**
	 * Retrieves the current frame number
	 * @return Current frame number
	 */
	int getFrameNum();

	/**
	 * Seek animation to given frame number
	 * @param frameNum - Frame to jump to
	 */
	void setFrameNum(int frameNum);

	/**
	 * Returns how many times the current animation has been played
	 * @return Play through count
	 */
	int getPlaythroughCount();

	/**
	 * Resets the playthrough count of the current animation
	 */
	void resetPlaythroughCount();

	/**
	 * Set whether to play the animation backwards. Note that playing backwards will still INcrement the play counter
	 * @param direction - True to play backwards
	 */
	void playBackwards(bool direction);

	/**
	 * Check whether the animation is playing backwards
	 * @return True if backwards
	 */
	bool playBackwards();
};

#endif //_ANIMATION_H_