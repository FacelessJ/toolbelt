#include "animation.hpp"
#include "animationstrip.hpp"


Animation::Animation()
{
	currentAnimation = 0;
	duration = 1;
	timer = 0;
	frameTime = 1;
	paused = false;
	backwards = false;
}

Animation::Animation(const Animation& rhs)
{
	for(unsigned int i = 0; i < strips.size(); i++)
	{
		delete strips[i];
	}
	strips.clear();
	for(unsigned int i = 0; i < rhs.strips.size(); i++)
	{
		strips.push_back(new AnimationStrip((*rhs.strips[i])));
	}

	currentAnimation = rhs.currentAnimation;
	duration = rhs.duration;
	timer = rhs.timer;
	frameTime = rhs.frameTime;
	paused = rhs.paused;
	backwards = rhs.backwards;
}

Animation& Animation::operator=(const Animation& rhs)
{
	for(unsigned int i = 0; i < strips.size(); i++)
	{
		delete strips[i];
	}
	strips.clear();
	for(unsigned int i = 0; i < rhs.strips.size(); i++)
	{
		strips.push_back(new AnimationStrip((*rhs.strips[i])));
	}

	currentAnimation = rhs.currentAnimation;
	duration = rhs.duration;
	timer = rhs.timer;
	frameTime = rhs.frameTime;
	paused = rhs.paused;
	backwards = rhs.backwards;
	return *this;
}

Animation::~Animation()
{
	for(unsigned int i = 0; i < strips.size(); i++)
		delete strips[i];
	strips.clear();
}

int Animation::loadStrip(int x, int y, int frameWidth, int frameHeight, int numFrames)
{
	strips.push_back(new AnimationStrip());
	strips[strips.size() - 1]->loadAnimationStrip(x, y, frameWidth, frameHeight, numFrames);
	return strips.size() - 1;
}

void Animation::pause()
{
	paused = true;
}

void Animation::resume()
{
	paused = false;
}

Rect Animation::getFrame()
{
	return strips[currentAnimation]->getCurrentFrame();
}

Rect Animation::getFrame(int index)
{
	return strips[currentAnimation]->getFrame(index);
}

Rect Animation::getNextFrame()
{
	return strips[currentAnimation]->getNextFrame();
}

void Animation::getFrameCoord(TexCoord coords[4])
{
	Rect coordRect = strips[currentAnimation]->getCurrentFrameCoord();
	coords[0] = TexCoord((float)coordRect.left, (float)coordRect.top);
	coords[1] = TexCoord((float)coordRect.left, (float)coordRect.height);
	coords[2] = TexCoord((float)coordRect.width, (float)coordRect.height);
	coords[3] = TexCoord((float)coordRect.width, (float)coordRect.top);
	return;
}

void Animation::getFrameCoord(TexCoord coords[4], int index)
{
	Rect coordRect = strips[currentAnimation]->getFrameCoord(index);
	coords[0] = TexCoord((float)coordRect.left, (float)coordRect.top);
	coords[1] = TexCoord((float)coordRect.left, (float)coordRect.height);
	coords[2] = TexCoord((float)coordRect.width, (float)coordRect.height);
	coords[3] = TexCoord((float)coordRect.width, (float)coordRect.top);
	return;
}

void Animation::getNextFrameCoord(TexCoord coords[4])
{
	Rect coordRect = strips[currentAnimation]->getNextFrameCoord();
	coords[0] = TexCoord((float)coordRect.left, (float)coordRect.top);
	coords[1] = TexCoord((float)coordRect.left, (float)coordRect.height);
	coords[2] = TexCoord((float)coordRect.width, (float)coordRect.height);
	coords[3] = TexCoord((float)coordRect.width, (float)coordRect.top);
	return;
}

void Animation::update(float dt)
{
	if(!paused)
	{
		timer += dt;
		if(timer > frameTime)
		{
			strips[currentAnimation]->advanceStrip(backwards);
			timer = 0;
		}
	}
}

void Animation::setAnimationDuration(float seconds)
{
	duration = seconds;
	frameTime = duration / strips[currentAnimation]->getNumFrames();
}

float Animation::getAnimationDuration()
{
	return duration;
}

bool Animation::isPaused()
{
	return paused;
}

int Animation::getCurrentAnimation()
{
	return currentAnimation;
}

void Animation::setCurrentAnimation(int index)
{
	if(index >= 0 && index < (signed)strips.size())
	{
		currentAnimation = index;
		frameTime = duration / strips[currentAnimation]->getNumFrames();
	}
}

int Animation::getFrameNum()
{
	return strips[currentAnimation]->getFrameCounter();
}

void Animation::setFrameNum(int frameNum)
{
	strips[currentAnimation]->seekStrip(frameNum);
}

int Animation::getPlaythroughCount()
{
	return strips[currentAnimation]->numTimesPlayed();
}

void Animation::resetPlaythroughCount()
{
	strips[currentAnimation]->resetPlayCount();
}

void Animation::playBackwards(bool direction)
{
	backwards = direction;
}

bool Animation::playBackwards()
{
	return backwards;
}