////////////////////////////////////////////////////////////
//
// Virtual Input System
// Copyright (C) 2009-2011 Josh Brown (josh@facelessj.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

/**
 * @file
 * @author Josh Brown
 * @brief These classes provide a simple way to store 2D arrays. Internally, stores them in 1D.
 * Also provides function to resize the grid in O(n) time.
 * Can access the array either using the traditional subscript operator [], or by using the ::at(x, y) function (0 indexed).
 * Both approaches allow retrieval/writing to the array. Be aware, neither approach does bounds checking for you
 * in any sense. For example, if the width is 5, and you try to access ::at(5, 0), it will access the first cell on the second
 * row, instead of throwing an error that x > row width.
 */

namespace jtb
{
	/**
	 * The anchor defines where the original grid is placed on the new grid. It works exactly the same as Photoshop's resize canvas tool.
	 * Middle anchors will distribute rows/columns evenly. For odd increases, the extra row/column will be added/subtracted from the right/bottom side
	 * of the 2D array.
	 * To demonstrate, a 2*2 array getting increased to a 5*4 array, using a MIDDLEMIDDLE anchor:
	 * o o      n n n n n
	 * o o  ==> n o o n n
	 *          n o o n n
	 *          n n n n n
	 * Default anchor is ANCHOR_TOPLEFT (When a row's length is increased, the new cells are appended to the end of the row, and when
	 * a column's length is increased, new cells are appended to the end of the column)
	 */
	enum RESIZEANCHOR
	{
		ANCHOR_TOPLEFT,
		ANCHOR_TOPMIDDLE,
		ANCHOR_TOPRIGHT,
		ANCHOR_MIDDLELEFT,
		ANCHOR_MIDDLEMIDDLE,
		ANCHOR_MIDDLERIGHT,
		ANCHOR_BOTTOMLEFT,
		ANCHOR_BOTTOMMIDDLE,
		ANCHOR_BOTTOMRIGHT,
	};

	/**
	 * @brief Class that handles storing a 2D array in 1D. Provides handy resizing function as well
	 */
	template <class T> class Array2D
	{
	public:
		/** 
		 * Creates a 1D array to handle a width*height 2D array
		 * @param width - Width of 2D array
		 * @param height - Height of 2D array
		 */
		Array2D(int width, int height) : w(width), h(height) {grid = new T[width*height]; }

		/**
		 * Destroys the array
		 */
		~Array2D() { delete [] grid; }

		/**
		 * Element access functions
		 */
		T& operator[](int index) { return grid[index]; } 
		const T& operator[](int index) const { return grid[index]; }

		T& at(int x, int y) { return grid[y * w + x]; }
		const T& at(int x, int y) const { return grid[y * w + x]; }

		/**
		 * Data polling functions
		 */
		int width() { return w; }
		int height() { return h; }
		int size() { return w * h; }

		/**
		 * Function to resize grid to new dimensions with defined anchor (explained above
		 */
		void resizeGrid(int width, int height, RESIZEANCHOR anchor = ANCHOR_TOPLEFT)
		{
			int widthDiff = width - w;
			int heightDiff = height - h;
			int leftMod = abs(widthDiff), rightMod = 0, topMod = abs(heightDiff), bottomMod = 0;
			if(anchor % 3 == 0)
			{
				leftMod = 0; rightMod = abs(widthDiff);
			}
			else if(anchor % 3 == 1)
			{
				leftMod = abs(widthDiff) >> 1; rightMod = abs(widthDiff) >> 1;
				if(abs(widthDiff) & 0x01) rightMod++;
			}
			if(anchor / 3 == 0)
			{
				topMod = 0;	bottomMod = abs(heightDiff);
			}
			else if (anchor / 3 == 1)
			{
				topMod = abs(heightDiff) >> 1; bottomMod = abs(heightDiff) >> 1;
				if(abs(heightDiff) & 0x01) bottomMod++;
			}

			T * tempGrid = new T[width * height];

			int minl = (widthDiff < 0 ? minl = leftMod : 0);
			int k = 0, l = minl;
			if(heightDiff < 0) k += topMod;

			int topBound = heightDiff > 0 ? topMod : 0;
			int botBound = heightDiff > 0 ? height - bottomMod : height;
			int leftBound = widthDiff > 0 ? leftMod : 0;
			int rightBound = widthDiff > 0 ? width - rightMod : width;

			for(int i = 0; i < height; i++)
			{
				for(int j = 0; j < width; j++)
				{
					if(i < topBound || i >= botBound || j < leftBound || j >= rightBound)
						tempGrid[i * width + j] = 0;
					else
					{
						tempGrid[i * width + j] = grid[k * w + l];
						l++;
					}
				}
				if(!(heightDiff > 0 && i < topMod))
					k++;
				l = minl;
			}

			delete [] grid;
			grid = tempGrid;
			w = width;
			h = height;
		}

	private:
		int w;
		int h;

		T* grid;

		//Prevent unwanted copying
		Array2D(const Array2D<T>&);
		Array2D& operator = (const Array2D<T>&);
	};

	/**
	 * @brief Class that handles storing a 2D array of pointers in 1D. Provides handy resizing function as well.
	 * Note that accessing it returns the pointer, so you will need to deference.
	 */
	template <class T> class Array2D <T*>
	{
	public:
		Array2D(int width, int height, bool initialiseValues = true) : w(width), h(height), deepDestroy(true)
		{
			grid = new T*[width*height]; 
			if(initialiseValues) 
				for(int i = 0; i < width*height; i++) 
					grid[i] = new T;
		}

		/**
		 * Destroys the array. Including freeing each pointer
		 */
		~Array2D() 
		{ 
			if(deepDestroy)
			{
				for(int i = 0; i < w * h; i++) 
					delete grid[i]; 
				memset(grid, 0, w * h * sizeof(T*));
			}
			delete [] grid; 
		}


		/**
		 * Element access functions
		 */
		T*& operator[](int index) { return grid[index]; } 
		const T*& operator[](int index) const { return grid[index]; }

		T*& at(int x, int y) { return grid[y * w + x]; }
		const T*& at(int x, int y) const { return grid[y * w + x]; }

		/**
		 * Data polling functions
		 */
		int width() { return w; }
		int height() { return h; }
		int size() { return w * h; }

		/**
		 * Function to resize grid to new dimensions with defined anchor (explained above
		 */
		void resizeGrid(int width, int height, RESIZEANCHOR anchor = ANCHOR_TOPLEFT)
		{
			int* oldNewMapping = new int[w * h];
			memset(oldNewMapping, -1, w * h * sizeof(int));
			int widthDiff = width - w;
			int heightDiff = height - h;
			int leftMod = abs(widthDiff), rightMod = 0, topMod = abs(heightDiff), bottomMod = 0;
			if(anchor % 3 == 0)
			{
				leftMod = 0; rightMod = abs(widthDiff);
			}
			else if(anchor % 3 == 1)
			{
				leftMod = abs(widthDiff) >> 1; rightMod = abs(widthDiff) >> 1;
				if(abs(widthDiff) & 0x01) rightMod++;
			}
			if(anchor / 3 == 0)
			{
				topMod = 0;	bottomMod = abs(heightDiff);
			}
			else if (anchor / 3 == 1)
			{
				topMod = abs(heightDiff) >> 1; bottomMod = abs(heightDiff) >> 1;
				if(abs(heightDiff) & 0x01) bottomMod++;
			}

			T ** tempGrid = new T*[width * height];

			int minl = (widthDiff < 0 ? minl = leftMod : 0);
			int k = 0, l = minl;
			if(heightDiff < 0) k += topMod;

			int topBound = heightDiff > 0 ? topMod : 0;
			int botBound = heightDiff > 0 ? height - bottomMod : height;
			int leftBound = widthDiff > 0 ? leftMod : 0;
			int rightBound = widthDiff > 0 ? width - rightMod : width;

			for(int i = 0; i < height; i++)
			{
				for(int j = 0; j < width; j++)
				{
					if(i < topBound || i >= botBound || j < leftBound || j >= rightBound)
						tempGrid[i * width + j] = new T;
					else
					{
						tempGrid[i * width + j] = grid[k * w + l];
						oldNewMapping[k * w + l] = i * width + j;
						l++;
					}
				}
				if(!(heightDiff > 0 && i < topMod))
					k++;
				l = minl;
			}

			//Delete culled cells
			for(int i = 0; i < w * h; i++)
			{
				if(oldNewMapping[i] == -1)
				{
					delete grid[i];
				}
			}

			delete [] grid;
			grid = tempGrid;
			delete [] oldNewMapping;
			w = width;
			h = height;
		}

		/**
		 * Modifies/retrieves flag indicating whether the Array should free the pointers when it is destroyed,
		 * or if it should leave them intact and just destroy the array itself. By default, it will free the pointers,
		 * so you only need to worry about this if you want to keep the pointers around after deleting the array
		 */
		void destroyOnDelete(bool destroy) {deepDestroy = destroy; };
		bool destroyOnDelete() {return deepDestroy;};
	private:
		int w;
		int h;

		T** grid;

		//Prevent unwanted copying
		Array2D(const Array2D<T>&);
		Array2D& operator = (const Array2D<T>&);

		bool deepDestroy;
	};
};