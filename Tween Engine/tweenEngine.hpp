////////////////////////////////////////////////////////////
//
// Tween Engine
// Copyright (C) 2009-2011 Josh Brown (josh@facelessj.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

/**
* @file
* @author Josh Brown
* @brief Handles interpolations of various sorts
*/

#ifndef _TWEENENGINE_H_
#define _TWEENENGINE_H_

#include "Iw2D.h"

/** 
 * 2D vector for convenience tweening functions. Requires direct access to .x and .y member variables,
 * as well as a constructor that takes x and y arguments
 */
typedef CIwFVec2 TweenVec2;

/**
 * Simple static class to handle tweening/interpolation calculations.
 * Examples of the various tweens can be found at
 * http://sol.gfxile.net/interpolation/
 */
class TweenEngine
{
private:
	/**
	 * Private constructor
	 */
	TweenEngine();

	static inline float smoothStep(float x);
	static inline float catMullRomCalc(float t, float p0, float p1, float p2, float p3);
public:
	/** Simple linear interpolation from a to b
	 * @param start - Start position
	 * @param end - End position
	 * @param t - Time stamp to get interpolated position at. 0 = a, 1 = b
	 * @return Linearly interpolated position from point start, to point end, at time t
	 */
	static float lerp(float start, float end, float t);
	static TweenVec2 lerp(TweenVec2 start, TweenVec2 end, float t);
	
	/** SmoothStep interpolation from a to b */
	static float smoothStep(float start, float end, float t);
	static TweenVec2 smoothStep(TweenVec2 start, TweenVec2 end, float t);

	/** Squared interpolation from a to b */
	static float squared(float start, float end, float t);
	static TweenVec2 squared(TweenVec2 start, TweenVec2 end, float t);

	/** Inverse Squared interpolation from a to b */
	static float invSquared(float start, float end, float t);
	static TweenVec2 invSquared(TweenVec2 start, TweenVec2 end, float t);

	/** Triangle interpolation with given amplitude and period, allowing for horizontal and vertical shifting */
	static float triangle(float amp, float period, float t, float xShift = 1, float yShift = 9999);

	/** CatMull-Rom Spline interpolation from a to b
	 * @param start - Position at t = 0
	 * @param end - Position at t = 1
	 * @param t - Time stamp to retrieve
	 * @param p0 - Anchor before start
	 * @param p3 - Anchor after end
	 * @return Interpolated position from start to end at time t
	 */
	static float catMullRom(float start, float end, float t, float p0, float p3);
	static TweenVec2 catMullRom(TweenVec2 start, TweenVec2 end, float t, TweenVec2 p0, TweenVec2 p3);

	/** Weight average movement from the current position to a target position
	 * @param current - Current position to interpolate from
	 * @param end - Target position
	 * @param slowDown - slowDown factor
	 * @return Next position to move to
	 */
	static float weightedAvg(float current, float end, float slowDown);
	static TweenVec2 weightedAvg(TweenVec2 current, TweenVec2 end, float slowDown);

};

#endif //_TWEENENGINE_H_