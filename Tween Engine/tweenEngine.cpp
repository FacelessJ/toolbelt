#include <cmath>
#include "tweenEngine.h"

TweenEngine::TweenEngine()
{

}

float TweenEngine::lerp(float start, float end, float t)
{
	return (start * (1 - t) + end * t);
}

TweenVec2 TweenEngine::lerp(TweenVec2 start, TweenVec2 end, float t)
{
	return CIwFVec2(start.x * (1 - t) + end.x * t, start.y * (1 - t) + end.y * t);
}


float TweenEngine::weightedAvg(float current, float end, float slowDown)
{
	return ((current * (slowDown - 1)) + end) / slowDown;
}

TweenVec2 TweenEngine::weightedAvg(TweenVec2 current, TweenVec2 end, float slowDown)
{
	return TweenVec2(((current.x * (slowDown - 1)) + end.x) / slowDown, ((current.y * (slowDown - 1)) + end.y) / slowDown);
}

inline float TweenEngine::smoothStep(float x)
{
	return (x * x * (3 - 2 * x));
}

float TweenEngine::smoothStep(float start, float end, float t)
{
	return (start * smoothStep(1 - t) + end * smoothStep(t));
}

TweenVec2 TweenEngine::smoothStep(TweenVec2 start, TweenVec2 end, float t)
{
	float invSmooth = smoothStep(1 - t);
	float smooth = smoothStep(t);
	return TweenVec2(start.x * invSmooth + end.x * smooth, start.y * invSmooth + end.y * smooth);
}

float TweenEngine::squared(float start, float end, float t)
{
	return start * (1 -  t * t) + end * t * t;
}

TweenVec2 TweenEngine::squared(TweenVec2 start, CIwFVec2 end, float t)
{
	float invSqr = (1 - t * t);
	return TweenVec2(start.x * invSqr + end.x * t * t, start.y * invSqr + end.y * t *t);
}


float TweenEngine::invSquared(float start, float end, float t)
{
	float aSqr = (1 - t) * (1 - t);
	return start * aSqr + end * (1 - aSqr);
}

TweenVec2 TweenEngine::invSquared(TweenVec2 start, TweenVec2 end, float t)
{
	float aSqr = (1 - t) * (1 - t);
	return TweenVec2(start.x * aSqr + end.x * (1 - aSqr), start.y * aSqr + end.y * (1 - aSqr));
}

inline float TweenEngine::catMullRomCalc(float t, float p0, float p1, float p2, float p3)
{
	return 0.5f * (
              (2 * p1) +
              (-p0 + p2) * t +
              (2 * p0 - 5 * p1 + 4 * p2 - p3) * t * t +
              (-p0 + 3 * p1 - 3 * p2 + p3) * t * t * t
              );
}

float TweenEngine::catMullRom(float start, float end, float t, float p0, float p3)
{
	float v = catMullRomCalc(t, p0, start, end, p3);
	return v;
}

TweenVec2 TweenEngine::catMullRom(TweenVec2 start, TweenVec2 end, float t, TweenVec2 p0, TweenVec2 p3)
{
	float vx = catMullRomCalc(t, p0.x, start.x, end.x, p3.x);
	float vy = catMullRomCalc(t, p0.y, start.y, end.y, p3.y);
	return TweenVec2(vx, vy);
}

float TweenEngine::triangle(float amp, float period, float t, float xShift, float yShift)
{
	if(yShift == 9999)
		yShift = amp;

	float modRes = (4/period*t-xShift)-floor((4/period*t-xShift)/4)*4;
	return amp * fabs(modRes - 2) - yShift;
}