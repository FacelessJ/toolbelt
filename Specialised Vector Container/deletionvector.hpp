////////////////////////////////////////////////////////////
//
// Vector Container (O(1) deletion)
// Copyright (C) 2009-2011 Josh Brown (josh@facelessj.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

/**
 * @file
 * @author Josh Brown
 * @brief This a vector container with a slight twist. The order of the items in the container are not
 * guaranteed, however, all deletions from the vector (no matter their location) will happen in constant
 * time. Due to the fact that the  order of elements changes often, there is no need for an insert function, 
 * as it would be just as effective to insert the element at the end of the container.
 *
 * This container was designed for processing portions of a large number of objects. For example, in a game
 * one typically has a collision manager that operates on every collidable in the game and checks for collisions.
 * This container can be used to hold the bounding data for each item, instead of the collision manager polling each
 * item individually. If a copy of the bounding data is given, rather than a pointer to the data, the container
 * will have spatial locality, and should significantly reduce cache misses, giving a large boost
 * in performance, particularly on large containers (as found in collision managers).
 *
 * When data is entered into the container, a VectorItem(T) will be returned. (*VectorItem(T))->value will then
 * always point to that piece of data in the container, even though ordering is not guaranteed. 
 *
 * Will automatically double the size of the container if you exceed the capacity of it.
 *
 * This container provides the [] operator for random access (In a kind of literal sense, due to reordering,
 * however, is still useful for iterating over the entire container).
 *
 * Example usage:
 * DeletionVector<int> myVector;
 * VectorItem(int) elementa = myVector.push_back(1);
 * VectorItem(int) elementb = myVector.push_back(2);
 * VectorItem(int) elementc = myVector.push_back(3);
 * ...
 * (*elementc)->value = 5;
 * myVector.erase(elementb);
 * //myVector now contains [1, 5]
 * //elementb is now a null reference
 * //elementa and c still point to their related elements
 */

#ifndef DELETIONVECTOR
#define DELETIONVECTOR

#include <new>
#include <vector>

/**
 * @brief Each element in the container.
 */
template <class T> struct VectorElement{
	VectorElement<T>** key;
	T value;
};

/** Macro to make accessing elements easier */
#define VectorItem(T) VectorElement<T>**

/**
 * @brief Templated vector container, with O(1) deletion time
 */
template <class T> class DeletionVector
{
private:
	/** Elements of the container */
	VectorElement<T> * elements;

	/** Size tracker for element array */
	int numElements;

	/** Capacity of element array */
	int containerSize;

	/** Keeps a copy of all used keys, so that we can delay deleting them until the vector goes out of scope
	 * Unfortunately this means inserting/deleting a LOT of items will lead to "leaking" keys until the vector is
	 * deleted. However, can call DeletionVector::garbageCollect(). But note this will cause problems with dereferencing
	 * VectorItems for deleted items, because instead of accessing a null pointer or the null element, both of which
	 * can be checked, it will instead access a deleted pointer and whatever value your computer leaves it as. (i.e 0xfeeefeee, not 0x0)
	 */
	std::vector<VectorItem(T)> keys;

	/** The null default element */
	VectorElement<T> nullElement;

	/** True if using the null element */
	bool useNull;

public:
	/**
	 * Default constructor
	 */
	DeletionVector()
	{
		containerSize = 64;
		elements = new VectorElement<T>[containerSize];
		numElements = 0;

		nullElement.key = new VectorElement<T>*;
		(*nullElement.key) = &nullElement;
		useNull = false;
	};

	/**
	 * Initialising constructor. Use if you know how big you want the container
	 */
	DeletionVector(int size)
	{
		containerSize = size;
		elements = new (std::nothrow) VectorElement<T>[containerSize];
		if(elements == 0)
		{
			containerSize = 64;
			elements = new VectorElement<T>[containerSize];
		}
		numElements = 0;

		nullElement.key = new VectorElement<T>*;
		(*nullElement.key) = &nullElement;
		useNull = false;
	};

	/**
	 * Destructor. Note that once the destructor is called, all external VectorItem(T)'s will become useless
	 * and will cause crashes if you use them.
	 * They all point to (*element[i].key), but I'm not sure how to keep that zeroed after the key is deleted
	 * in delete keys[i]. It's probably not even wise to attempt such a thing, so just make sure you don't try
	 * to use a vector item after the vector has been deleted
	 */
	~DeletionVector()
	{
		for(int i = 0; i < numElements; i++)
		{
			(*elements[i].key) = 0;
			elements[i].key = 0;
		}
		for(unsigned int i = 0; i < keys.size(); i++)
			delete keys[i];
		if(elements)
			delete [] elements;
		elements = 0;

		delete nullElement.key;
	};

	/**
	 * Sets the null element to some value. Pass in some delimiting default value.
	 * @param object - Default element to use for deleted elements
	 */
	void setNullElement(T object)
	{
		nullElement.value = object;
	}

	/**
	 * Set whether or not to use a null object for items that get deleted out of the vector (Only works while the
	 * vector is still in scope). If using a null element, any access to the vector through a VectorItem belonging 
	 * to a deleted item will instead return results from a prespecified null element. If you don't want to use
	 * the null element, it will instead clear the pointer that the VectorItem points to, meaning you should then
	 * always do a if(*myVectorItem) to make sure it's not null
	 * @param state - Whether to use the null object
	 */
	void useNullElement(bool state)
	{
		useNull = state;
	}

	/**
	 * Checks whether the vector is currently using the null element or instead clearing pointers
	 * @return - True if using the null element
	 */
	bool useNullElement()
	{
		return useNull;
	}

	/**
	 * Clears all elements from the vector. Does not need to be called before deleting the vector
	 */
	void clear()
	{
		for(int i = 0; i < numElements; i++)
		{
			if(useNull)
				(*elements[i].key) = &nullElement;
			else
				(*elements[i].key) = 0;
			elements[i].key = 0;
		}
		numElements = 0;
	}

	/**
	 * Resizes the vector. O(n) complexity, so try not to call it often. Will be called automatically
	 * if needed.
	 * @param size - New capacity
	 * @return True on successful resize
	 */
	bool resize(int size)
	{
		VectorElement<T> * newElements = new (std::nothrow) VectorElement<T>[size];
		if(newElements == 0)
			return false;
		if(size < numElements)
		{
			for(int i = size; i < numElements; i++)
			{
				if(useNull)
					(*elements[i].key) = &nullElement;
				else
					(*elements[i].key) = 0;
				elements[i].key = 0;
			}
			numElements = size;
		}
		for(int i = 0; i < numElements; i++)
		{
			newElements[i] = elements[i];
			newElements[i].key = elements[i].key;
			(*newElements[i].key) = &newElements[i];
		}
		delete [] elements;
		elements = newElements;
		containerSize = size;
		return true;
	};

	/**
	 * Retrieves the total capacity of the vector (not how many items it currently has)
	 * @return Vector capacity
	 */
	int capacity()
	{
		return containerSize;
	};

	/**
	 * Retrieves how many elements are currently in the vector
	 * @return Number of elements
	 */
	int size()
	{
		return numElements;
	};

	/**
	 * Insert an element to the end of the vector. If the vector is not large enough, it will
	 * resize the vector to twice its current capacity.
	 * @param object - Item inserting into the vector
	 * @return Accessor to that specific item, or null if a resize was triggered, but failed
	 */
	VectorElement<T>** push_back(T object)
	{
		if(numElements >= containerSize)
		{
			if(!resize(2 * containerSize))
				return 0;
		}

		elements[numElements].value = object;
		elements[numElements].key = new VectorElement<T>*;
		(*elements[numElements].key) = &elements[numElements];
		keys.push_back(elements[numElements].key);
		return elements[numElements++].key;
	};

	/**
	 * Erase specific element from the vector
	 * @param object - Object to remove
	 */
	void erase(VectorElement<T>** &object)
	{
		int index = (int)((float)((*object)-elements));
		if(index < 0 || index > numElements)
			return;
		if(useNull)
			(*elements[index].key) = &nullElement;
		else
		{
			(*elements[index].key) = 0;
			object = 0;
		}
		elements[index].key = 0;

		numElements--;
		//If removed any element except last, rearrange
		if(numElements != index)
		{
			*(elements[numElements].key) = &elements[index];
			elements[index] = elements[numElements];
		}
	};
	
	/**
	 * Access element at index. If any elements are erased between accessing the same index,
	 * cannot guarantee it will access the same item. However, can check if it is same
	 * item by comparing the ret.key with the old value
	 * @param index - Index to access
	 * @return Element in array
	 */
	VectorElement<T>& operator[](int index) 
	{
		return elements[index]; 
	
	} 

	/**
	 * Access element at index. If any elements are erased between accessing the same index,
	 * cannot guarantee it will access the same item. However, can check if it is same
	 * item by comparing the ret.key with the old value
	 * @param index - Index to access
	 * @return Element in array
	 */
	const VectorElement<T>& operator[](int index) const 
	{
		return elements[index]; 
	}

	/**
	 * Deletes all the keys and thus vector items for elements no longer in the vector.
	 * BE WARNED that this INVALIDATE all external references into the array for deleted objects.
	 * They will no longer access a null pointer or the null element. Use at own risk as inappropriate use
	 * can cause crashing
	 */
	void garbageCollect()
	{
		VectorElement<T>* compare = 0;
		if(useNull)
			compare = &nullElement;
		for(unsigned int i = 0; i < keys.size(); i++)
		{
			if((*keys[i]) == compare)
			{
				delete keys[i];
				keys.erase(keys.begin() + i);
				i--;
			}
		}
		
	}


};

#endif //DELETIONVECTOR
