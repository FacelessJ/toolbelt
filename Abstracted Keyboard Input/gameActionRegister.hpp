////////////////////////////////////////////////////////////
//
// Virtual Input System
// Copyright (C) 2009-2011 Josh Brown (josh@facelessj.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

/**
 * @file
 * @author Josh Brown
 * @brief Defines a mapping between game action names (i.e "jump") and a flag value used
 * in the virtual input class. Every action corresponds to a single bit in a standard
 * integer (generally 32-bit). This means the entire action list (What's active and what
 * is not) can be stored in an int, but on the downside, this restricts the game to only
 * have 32 (Or however many bits the int is) input actions. Most games don't need more than
 * this (For instance, can save on actions by reusing actions in different parts of the game,
 * like the menu and in-game. Just means in one of the area's source code, the action will
 * be misnamed). If more actions are absolutely needed, then a larger datatype could be used.
 *
 * The available actions can be set in the constructor.
 */

#ifndef _GAMEACTIONREGISTER_H_
#define _GAMEACTIONREGISTER_H_

#include <map>
#include <string>

/**
 * @brief Stores all the available actions by string name and bitwise flags
 */
class GameActionRegister
{
private:
	/**
	 * Map storing link between action name and its bitwise flag
	 */
	std::map<std::string, int> actionRegister;


	/** Singleton reference to the game action register */
	static GameActionRegister* instance;

	/**
	 * Initialises map to contain all available game actions
	 */
	GameActionRegister();

public:
	
	/**
	 * Returns singleton pointer
	 * @return singleton pointer
	 */
	static GameActionRegister* Instance();


	/**
	 * Destructor
	 */
	~GameActionRegister()
	{
		actionRegister.erase(actionRegister.begin(), actionRegister.end());
		actionRegister.clear();

	};

	/**
	 * Returns the flag number for the requested action
	 * @param action - Action to search for
	 * @return action flag
	 */
	int getActionFlag(std::string action);

	/**
	 * Performs reverse lookup to find action name string based on flag number.
	 * Will always return a unique value since all flags should be unique
	 * @param flag - Flag searching for
	 * @return Action name
	 */
	std::string getActionName(int flag);

	/**
	 * Returns a read only reference to the actions
	 * @return Read only pointer to actions map
	 */
	const std::map<std::string, int> * const getActions()
	{
		return &actionRegister;
	}
};

#endif // _GAMEACTIONREGISTER_H_