////////////////////////////////////////////////////////////
//
// Virtual Input System
// Copyright (C) 2009-2011 Josh Brown (josh@facelessj.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

/**
 * @file
 * @author Josh Brown
 * @brief This class sets up an interface between the physical input (keyboard + mouse) and action
 * aliases in game. At the start of every frame, getInput() should be called ONCE. This polls the state of
 * the keyboard and mouse, and sets the necessary flags. For the rest of the cycle, the game code can
 * use the actionActive(), actionPressed() and actionReleased() functions to determine the state of
 * any of the actions defined in the gameActionRegister.
 * i.e One would use:
 *   if(VirtualInput::Instance()->actionActive("JUMP"))
 *     doJumpCodeHere;
 * The "current" input state will remain constant during the entire frame, as it is only updated 
 * with getInput(). At the end of a frame, endInputCycle() should be called (ONCE), to save the 
 * current input state, for comparison purposes in the next frame. (i.e to determine actionPressed()
 * and actionReleased()).
 *
 * Note, this system will work 99% out of the box, but there are changes that need to be made by
 * whoever is using this code. This system assumes the keyboard/mouse state are represented in enums
 * (DirectX does this, as does SFML. Both of which this system has been tested in). The enum values
 * (i.e DIK_A or sf::Keyboard::A) are stored in the key value for the bindings map. The input system
 * treats them as a single enum by "appending" the mouse enum to the keyboard enum (Basically, offset
 * the mouse enum by how many entries are the keyboard enum). These "single-enum" values
 * are used in the getInput() function to determine which actions are active. When using
 * bindKey() (Particularly in the assignDefaults() function), you should use the naming system for
 * whatever API you're using. Finally, the only other piece of code that needs to be changed is in
 * getInput(). There are four lines which use API-specific calls (The calls which physically poll the
 * input devices). Change these as needed.
 *
 */


#ifndef _VIRTUALINPUT_H_
#define _VIRTUALINPUT_H_

/**
 * Current version of the input system. If different from the player's profile file, will set their keys to the default binds specified here.
 */
#define INPUTVERSION 0x01

#include "gameActionRegister.h"

/**
 * @brief Class which binds keys to game actions and can be polled to find which actions are being activated
 */
class VirtualInput
{
private:
	/**
	 * Map storing key and action bindings. Key is the unique key.
	 * This means multiple keys can do same action, but each key can only do one action
	 * std::map<PhysicalKey, GameAction>
	 */
	std::map<int, int> bindings;

	/**
	 * Member variable which stores the bit string of currently active game actions
	 */
	int flags;

	/**
	 * Member variable which stores the bit string of game actions active in the last cycle
	 */
	int previousFlags;
	
	/** Singleton reference */
	static VirtualInput* instance;

	/**
	 * Initialises the class. Binds all keys to NULLACTION
	 */
	VirtualInput();

	/** Mouse x position */
	int mouseX;
	/** Mouse y position */
	int mouseY;
	/** Previous mouse x position */
	int prevMouseX;
	/** Previous mouse y position */
	int prevMouseY;
public:
	
	/**
	 * Clears all binds (Sets them to NULLACTION) and sets up some predefined bindings
	 */
	void assignDefaults();

	/**
	 * Returns a singleton pointer to the virtual input system
	 * @return Singleton pointer
	 */
	static VirtualInput* Instance();

	/**
	 * Destructor
	 */
	~VirtualInput();

	/**
	 * Binds key to action
	 * @param key - Keyboard key to bind (DirectX DIK_<KEY> constant)
	 * @param action - Action to bind to the key (from GameActionRegister)
	 * @return Returns true if overwrote a key
	 */
	bool bindKey(int key, int action);

	/**
	 * Unbinds the key from any action
	 * @param key - Key to assign to NULLACTION
	 */
	void unbindKey(int key);

	/**
	 * Returns a read only reference to the bindings
	 * @return Read only pointer to bindings map
	 */
	const std::map<int, int> * const getBinds()
	{
		return &bindings;
	}

	/**
	 * Unbinds all keys from the action
	 * @param action - Action to unbind
	 */
	void unbindAction(int action);

	/**
	 * Gives the input states to the virtual input system for processing
	 */
	void getInput();

	/**
	 * Saves the binds to the file "keybinds.ini"
	 */
	void saveBinds();

	/**
	 * Loads the binds out of the file "keybinds.ini"
	 */
	void loadBinds();

	/**
	 * Resets the virtual key map.
	 */
	void flush();

	/**
	 * Returns whether the action is currently active
	 * @param action - Action to check
	 * @return True if action is active
	 */
	bool actionActive(std::string action);

	/**
	 * Returns whether the action was just activated (Since last call to endInputCycle)
	 * @param action - Action to check
	 * @return True if action just activated
	 */
	bool actionPressed(std::string action);

	/**
	 * Returns whether the action was just deactivated (Was active before last endInputCycle and is no longer active)
	 * @param action - Action to check
	 * @return True if action just deactivated
	 */
	bool actionReleased(std::string action);

	/**
	 * Sets up the virtual key system to move onto the next cycle (Store previous keypresses, etc)
	 */
	void endInputCycle();

	/**
	 * Polls mouse x position
	 * @return x position of mouse
	 */
	int getMousePosX()
	{
		return mouseX;
	}

	/**
	 * Polls mouse y position
	 * @return y position of mouse
	 */
	int getMousePosY()
	{
		return mouseY;
	}

	/**
	 * Polls mouse position
	 * @return (x, y) position of mouse
	 */
	sf::Vector2i getMousePos()
	{
		return sf::Vector2i(mouseX, mouseY);
	}

	/**
	 * Polls how much the x position of the mouse has changed since last frame
	 * @return x delta of mouse
	 */
	int getMouseChangeX()
	{
		return mouseX - prevMouseX;
	}

	/**
	 * Polls how much the y position of the mouse has changed since last frame
	 * @return y delta of mouse
	 */
	int getMouseChangeY()
	{
		return mouseY - prevMouseY;
	}

	/**
	 * Polls how much the position of the mouse has changed since last frame
	 * @return (x, y) delta of mouse
	 */
	sf::Vector2i getMouseChange()
	{
		return sf::Vector2i(mouseX - prevMouseX, mouseY - prevMouseY);
	}
};


#endif