#include "../include/gameActionRegister.h"

GameActionRegister *GameActionRegister::instance = 0;

GameActionRegister* GameActionRegister::Instance()
{
	//static GameActionRegister thisInstance;
	//return &thisInstance;
	if (instance == 0) {
        instance = new GameActionRegister;
    }
    return instance;
}

GameActionRegister::GameActionRegister()
{
	//Initialises the game action register. Only needs to be done once
	//Make sure data is unique (Both key and data should be unique)

	//Some sample actions
	actionRegister.insert(std::pair<std::string,int>("NULLACTION", 0x00));
	actionRegister.insert(std::pair<std::string,int>("MOUSE1", 0x01));
	actionRegister.insert(std::pair<std::string,int>("TEST", 0x02));
	actionRegister.insert(std::pair<std::string,int>("MOVELEFT", 0x04));
	actionRegister.insert(std::pair<std::string,int>("MOVERIGHT", 0x08));
	actionRegister.insert(std::pair<std::string,int>("JUMP", 0x10));

	//Note that each of the values represents a different bit in the binary representation of
	//an int. This is vital to the operation of the virtual input system. Whenever adding a
	//new action, simply double the last action's value, all the way up to 0x80000000. To those
	//not savvy in hexadecimal, just follow the pattern of 1, 2, 4, 8. When you reach 8, go back
	//to 1 and add another 0 to the end of the number. i.e 0x1000 -> 0x2000 -> 0x4000 -> 0x8000 -> 0x10000
}

int GameActionRegister::getActionFlag(std::string action)
{
	static std::map<std::string,int>::iterator it;

	//Get action flag number
	it = actionRegister.find(action);

	//Return flag if the action was found
	if(it != actionRegister.end())
		return it->second;

	//Else return the NULLACTION flag
	return 0x00;
}

std::string GameActionRegister::getActionName(int flag)
{
	static std::map<std::string,int>::iterator it;

	//Searches map for specific flag and returns the name of the action
	for(it = actionRegister.begin(); it != actionRegister.end(); it++)
	{
		if(it->second == flag)
			return it->first;
	}
	//If flag was not found, NULLACTION is returned
	return "NULLACTION";
}