#include "../include/virtualinput.h"


/** 
 * An offset value used to refer to mouse buttons in the keyboard domain.
 * i.e sf::Mouse::Left + MOUSEOFFSET will give the "key" value for the Left Mouse button. 
 * Basically adds the mouse button enum onto the end of the keyboard enum to be able to use them as a single enum
 */
#define MOUSEOFFSET sf::Keyboard::KeyCount

VirtualInput *VirtualInput::instance = 0;

VirtualInput::VirtualInput()
{
	assignDefaults();
	flags = previousFlags = 0;
	mouseX = mouseY = prevMouseX = prevMouseY = 0;
}

VirtualInput::~VirtualInput()
{
	delete GameActionRegister::Instance();
}
void VirtualInput::assignDefaults()
{
	flush();

	//Sample bindings
	bindKey(sf::Keyboard::A, GameActionRegister::Instance()->getActionFlag("TEST"));
	bindKey(sf::Mouse::Left + MOUSEOFFSET, GameActionRegister::Instance()->getActionFlag("MOUSE1"));
	bindKey(sf::Keyboard::Left, GameActionRegister::Instance()->getActionFlag("MOVELEFT"));
	bindKey(sf::Keyboard::Right, GameActionRegister::Instance()->getActionFlag("MOVERIGHT"));
	bindKey(sf::Keyboard::Space, GameActionRegister::Instance()->getActionFlag("JUMP"));
}

VirtualInput* VirtualInput::Instance()
{
	if (instance == 0) {
        instance = new VirtualInput;
    }
    return instance;
}

void VirtualInput::getInput()
{
	//Only needs to be initialised once.
	static std::map<int,int>::iterator it;
	int retVal = 0x0000;

	//Poll input here and fill an int with the ACTION0x defines.
	
	//Get mouse position - CHANGE TO API-SPECIFIC CALL
	mouseX = sf::Mouse::GetPosition((*ResourceManager::App)).x;
	mouseY = sf::Mouse::GetPosition((*ResourceManager::App)).y;

	//For all the actions in the bindings register, check if their key is pressed in the physical media
	for(it = bindings.begin(); it != bindings.end(); it++)
	{
		if(it->first < MOUSEOFFSET)
		{
			//CHANGE TO API-SPECIFIC CALL
			if(sf::Keyboard::IsKeyPressed(it->first))
				retVal |= it->second;
		}
		else
		{
			//CHANGE TO API-SPECIFIC CALL
			if(sf::Mouse::IsButtonPressed(it->first - MOUSEOFFSET))
				retVal |= it->second;
		}	
	}

	flags = retVal;
}

bool VirtualInput::bindKey(int key, int action)
{
	std::pair<std::map<int,int>::iterator,bool> returnVal;

	//Inserts the binding. If it already exists, returnVal.second will be false.
	returnVal = bindings.insert(std::pair<int,int>(key, action));

	//If was already in the map, overwrite it with the new value
	if(returnVal.second == false)
	{
		returnVal.first->second = action;
		return true;
	}
	return false;
}

void VirtualInput::unbindKey(int key)
{
	bindings.erase(key);
}

void VirtualInput::unbindAction(int action)
{
	std::map<int, int>::iterator bindIt;
	for(bindIt = bindings.begin(); bindIt != bindings.end(); bindIt++)
	{
		if(bindIt->second == action)
		{
			bindIt->second = 0x00;
		}
	}
}

void VirtualInput::saveBinds()
{
	FILE* fp;
	//Open file for saving
	fopen_s(&fp, "keybinds.ini", "w+");
	//If successful
	if(fp != NULL)
	{
		std::map<int,int>::iterator it;
		//Iterate through map and write each entry to file
		for(it = bindings.begin(); it != bindings.end(); it++)
		{
			//Don't bother saving unbound keys (bound to 0x00)
			if(it->second != 0x00)
				fprintf_s(fp, "%d %.32s\n", it->first, (GameActionRegister::Instance()->getActionName(it->second)).c_str());
		}
		fclose(fp);
	}
}

void VirtualInput::loadBinds()
{
	FILE* fp;
	//Open file for reading
	fopen_s(&fp, "keybinds.ini", "r+");
	//If successful
	if(fp != NULL)
	{
		int key;
		char string[33];
		while(!feof(fp))
		{
			fscanf_s(fp, "%d %s\n", &key, string, 33);
			bindKey(key, GameActionRegister::Instance()->getActionFlag(string));
		}
		fclose(fp);
	}
}

void VirtualInput::flush()
{
	//Bind all keys to NULLACTION
	for(int i = 0; i < 256; i++)
	{
		bindKey(i, 0x00);
	}
}

bool VirtualInput::actionActive(std::string action)
{
	if(flags & GameActionRegister::Instance()->getActionFlag(action))
		return true;
	return false;
}

bool VirtualInput::actionReleased(std::string action)
{
	if((previousFlags & GameActionRegister::Instance()->getActionFlag(action)) && !(flags & GameActionRegister::Instance()->getActionFlag(action)))
	{
		return true;
	}
	return false;
}

bool VirtualInput::actionPressed(std::string action)
{
	if(!(previousFlags & GameActionRegister::Instance()->getActionFlag(action)) && (flags & GameActionRegister::Instance()->getActionFlag(action)))
	{
		return true;
	}
	return false;
}

void VirtualInput::endInputCycle()
{
	previousFlags = flags;
	prevMouseX = mouseX;
	prevMouseY = mouseY;
}

